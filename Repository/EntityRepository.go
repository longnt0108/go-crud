package Repository

import (
	"database/sql"
	"go-curd/Entity"
)

type EntityRepository struct {
	db *sql.DB
}

func NewEntityRepository(db *sql.DB) *EntityRepository {
	return &EntityRepository{db: db}
}

func (r *EntityRepository) ListEntities() ([]Entity.Entity, error) {
	var entities []Entity.Entity

	rows, err := r.db.Query("SELECT id, name FROM entity")
	if err != nil {
		return entities, err
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			return
		}
	}(rows)

	for rows.Next() {
		var entity Entity.Entity
		if err := rows.Scan(&entity.ID, &entity.Name); err != nil {
			//http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return entities, err
		}
		entities = append(entities, entity)
	}

	return entities, err
}

func (r *EntityRepository) GetEntityByID(id int) (Entity.Entity, error) {
	var entity Entity.Entity
	err := r.db.QueryRow("SELECT id, name FROM entity WHERE id=?", id).Scan(&entity.ID, &entity.Name)

	return entity, err
}

func (r *EntityRepository) CreateEntity(entity Entity.Entity) (Entity.Entity, error) {
	result, err := r.db.Exec("INSERT INTO entity (name) VALUES (?)", entity.Name)
	if err != nil {
		return entity, err
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return entity, err
	}
	entity.ID = int(lastInsertID)
	return entity, err
}

func (r *EntityRepository) UpdateEntity(entity Entity.Entity) error {
	_, err := r.db.Exec("UPDATE entity SET name=? WHERE id=?", entity.Name, entity.ID)

	return err
}

func (r *EntityRepository) DeleteEntity(id int) error {
	_, err := r.db.Exec("DELETE FROM entity WHERE id=?", id)

	return err
}
