// main.go
package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/golang-jwt/jwt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"go-curd/Controller"
	"html/template"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"
)

var db *sql.DB

var (
	secretKey   string
	databaseURL string
)

func init() {
	// Load environment variables from .env file
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}

	// Assign the values of environment variables to the variables
	secretKey = os.Getenv("SECRET_KEY")
	databaseURL = os.Getenv("DATABASE_URL")
}

func main() {
	// Open a connection to the MySQL database
	var err error
	db, err = sql.Open("mysql", databaseURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Create the table if it doesn't exist
	if err := createTableIfNotExists(); err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()

	entityController := Controller.NewEntityController(db)

	// Define CRUD routes
	registerPublicHandler(r, "/", adminDashboard, []string{"GET"})
	registerPublicHandler(r, "/entities", entityController.ListEntities, []string{"GET"})
	registerPublicHandler(r, "/entities/show/{id}", entityController.GetEntity, []string{"GET"})
	registerPublicHandler(r, "/entities/create", entityController.CreateEntity, []string{"GET", "POST"})
	registerPublicHandler(r, "/entities/edit/{id}", entityController.UpdateEntity, []string{"GET", "POST"})
	registerPublicHandler(r, "/entities/delete/{id}", entityController.DeleteEntity, []string{"GET"})

	registerPublicHandler(r, "/auth", generateToken, []string{"GET"})
	registerProtectedHandler(r, "/entity", entityController.List, []string{"GET"})
	registerProtectedHandler(r, "/entity/show/{id}", entityController.GetEntity, []string{"GET"})
	registerProtectedHandler(r, "/entity/create", entityController.Create, []string{"POST"})
	registerProtectedHandler(r, "/entity/edit/{id}", entityController.Update, []string{"PUT"})
	registerProtectedHandler(r, "/entity/delete/{id}", entityController.Delete, []string{"DELETE"})

	http.Handle("/", r)
	fmt.Println("Server is running on :8080...")
	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		return
	}

	//queryElasticSearch("test", "fr")
}

func registerPublicHandler(r *mux.Router, path string, f func(http.ResponseWriter, *http.Request), methods []string) {
	for _, method := range methods {
		r.HandleFunc(path, f).Methods(method)
	}
}

func registerProtectedHandler(r *mux.Router, path string, f func(http.ResponseWriter, *http.Request), methods []string) {
	for _, method := range methods {
		r.Handle(path, jwtMiddleware(http.HandlerFunc(f))).Methods(method)
	}
}

func jwtMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		if tokenString == "" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		tokenString = strings.Replace(tokenString, "Bearer ", "", 1)
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			// Check the signing method
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf(" Invalid signing method: %v", token.Header["alg"])
			}
			return []byte(secretKey), nil // Provide your secret key here
		})

		if err != nil {
			fmt.Println(err)
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			// Token is valid
			//fmt.Println("User ID:", claims["user_id"])
			//fmt.Println("Expiration Time:", time.Unix(int64(claims["exp"].(float64)), 0))
			next.ServeHTTP(w, r)
		} else {
			// Token is invalid
			http.Error(w, "Invalid token", http.StatusUnauthorized)
		}

	})
}

// You can replace logic generateToken here
func generateToken(w http.ResponseWriter, r *http.Request) {
	var exp = time.Now().Add(time.Minute * 30)
	claims := jwt.MapClaims{
		"user_id": rand.Int(),
		"exp":     exp.Unix(), // Expiration time (30 minutes from now)
	}

	// Create a token with the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign the token with a secret key
	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		fmt.Println("Error signing token:", err)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	response := map[string]string{"type": "Bearer", "token": tokenString, "exp": exp.String()}
	outJson, err := json.Marshal(response)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	_, err = w.Write(outJson)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func queryElasticSearch(term string, language string) {
	esClient, err := createESClient()
	if err != nil {
		fmt.Printf("Error creating Elasticsearch client: %v\n", err)
		return
	}

	// Load the JSON query from the file
	query, err := loadTemplate("templates/ElasticSearch/news.json")
	if err != nil {
		fmt.Printf("Error loading query: %v\n", err)
		return
	}

	// Replace placeholders in the query
	query = strings.ReplaceAll(query, "%%SEARCH%%", term)
	query = strings.ReplaceAll(query, "%%LANG%%", language)

	// Perform the search
	res, err := esClient.Search(
		esClient.Search.WithContext(context.Background()),
		esClient.Search.WithIndex("news"),
		esClient.Search.WithBody(strings.NewReader(query)),
	)
	if err != nil {
		fmt.Printf("Error performing search: %v\n", err)
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Printf("Error performing search: %v\n", err)
			return
		}
	}(res.Body)

	// Handle the search response (e.g., read and parse the results)
	if res.IsError() {
		fmt.Printf("Error response: %s\n", res.Status())
		var errorResponse map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&errorResponse); err != nil {
			fmt.Printf("Error decoding Elasticsearch error response: %v\n", err)
		} else {
			fmt.Printf("Elasticsearch Error Reason: %s\n", errorResponse["error"].(map[string]interface{})["reason"])
		}
	} else {
		fmt.Printf("Search results: %s\n", res.String())
	}
}

func createESClient() (*elasticsearch.Client, error) {
	cfg := elasticsearch.Config{
		Addresses: []string{"http://127.0.0.1:9200"},
		// You can specify additional configuration options as needed.
	}

	esClient, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	return esClient, nil
}

func loadTemplate(filename string) (string, error) {
	// Read the JSON template from the file
	content, err := os.ReadFile(filename)
	if err != nil {
		return "", err
	}

	return string(content), nil
}

func adminDashboard(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("templates/dashboard.html")
	err := t.Execute(w, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}
}

func createTableIfNotExists() error {
	_, err := db.Exec(`
        CREATE TABLE IF NOT EXISTS entity (
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255)
        )
    `)
	return err
}
