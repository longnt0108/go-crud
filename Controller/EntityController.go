package Controller

import (
	"database/sql"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"go-curd/Entity"
	"go-curd/Repository"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type EntityController struct {
	db   *sql.DB
	repo *Repository.EntityRepository
}

func NewEntityController(db *sql.DB) *EntityController {
	repo := Repository.NewEntityRepository(db)
	return &EntityController{db: db, repo: repo}
}

func (ec *EntityController) ListEntities(w http.ResponseWriter, r *http.Request) {
	var entities []Entity.Entity
	entities, err := ec.repo.ListEntities()
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	renderTemplate(w, "list", entities)
}

func (ec *EntityController) List(w http.ResponseWriter, r *http.Request) {
	var entities []Entity.Entity
	entities, err := ec.repo.ListEntities()
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	jsonResponse(w, entities)
}

func (ec *EntityController) GetEntity(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	entityID := params["id"]
	id, err := strconv.Atoi(entityID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		panic(err)
	}

	var entity Entity.Entity
	entity, err = ec.repo.GetEntityByID(id)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	jsonResponse(w, entity)
}

func (ec *EntityController) CreateEntity(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		var entity Entity.Entity
		entity.Name = r.FormValue("name")

		entity, err := ec.repo.CreateEntity(entity)
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/entities", http.StatusFound)
	} else {
		renderTemplate(w, "create", nil)
	}
}

func (ec *EntityController) Create(w http.ResponseWriter, r *http.Request) {
	var entity Entity.Entity
	if err := parseJSONBody(r, &entity); err != nil {
		log.Println(err)
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}

	entity, err := ec.repo.CreateEntity(entity)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	jsonResponse(w, entity)
}

func (ec *EntityController) UpdateEntity(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var entity Entity.Entity
	entityID := params["id"]
	entity.ID, _ = strconv.Atoi(entityID)
	entity, _ = ec.repo.GetEntityByID(entity.ID)
	if r.Method == http.MethodPost {
		entity.Name = r.FormValue("name")
		err := ec.repo.UpdateEntity(entity)
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/entities", http.StatusFound)
	} else {
		renderTemplate(w, "edit", entity)
	}
}

func (ec *EntityController) Update(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var entity Entity.Entity
	entityID := params["id"]
	entity.ID, _ = strconv.Atoi(entityID)
	if err := parseJSONBody(r, &entity); err != nil {
		log.Println(err)
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}
	err := ec.repo.UpdateEntity(entity)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	jsonResponse(w, entity)
}

func (ec *EntityController) DeleteEntity(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	entityID := params["id"]

	id, err := strconv.Atoi(entityID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = ec.repo.DeleteEntity(id)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/entities", http.StatusFound)
}

func (ec *EntityController) Delete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	entityID := params["id"]

	id, err := strconv.Atoi(entityID)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = ec.repo.DeleteEntity(id)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func jsonResponse(w http.ResponseWriter, data interface{}) {
	outJson, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	_, err = w.Write(outJson)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func parseJSONBody(r *http.Request, v interface{}) error {
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(v)
}

func renderTemplate(w http.ResponseWriter, tmpl string, data interface{}) {
	t, _ := template.ParseFiles("templates/" + tmpl + ".html")
	err := t.Execute(w, data)
	if err != nil {
		log.Println(err.Error())
		return
	}
}
