# Use an official Go runtime as a parent image
FROM golang:1.21.1-alpine

# Install git
RUN apk add git

# Set the working directory inside the container
WORKDIR /app

# Copy the Go application source code into the container
COPY . .

# Set the GO111MODULE environment variable to "on" to enable Go modules
ENV GO111MODULE=on

# Build the Go application
RUN go build -o app .

# Install CompileDaemon
RUN go install -mod=mod github.com/githubnemo/CompileDaemon

# Define the command to run the Go application with CompileDaemon
CMD CompileDaemon -build="go build -o app ." -command="./app"
