package Entity

type Entity struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
