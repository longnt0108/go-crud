package Entity

type User struct {
	ID         int
	UserName   string
	UserSecret string
}
